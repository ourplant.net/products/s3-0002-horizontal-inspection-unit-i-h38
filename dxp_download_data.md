Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0002-horizontal-inspection-unit-i-h38).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |[de](https://gitlab.com/ourplant.net/products/s3-0002-horizontal-inspection-unit-i-h38/-/raw/main/01_operating_manual/S3-0002_A2_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0002-horizontal-inspection-unit-i-h38/-/raw/main/01_operating_manual/S3-0002_A2_Operating%20Manual.pdf)              |
| assembly drawing         |[en](https://gitlab.com/ourplant.net/products/s3-0002-horizontal-inspection-unit-i-h38/-/raw/main/02_assembly_drawing/s3-0002-a_i-h38.pdf)|
| circuit diagram          |[en](https://gitlab.com/ourplant.net/products/s3-0002-horizontal-inspection-unit-i-h38/-/raw/main/03_circuit_diagram/S3-0002-EPLAN-B.pdf)                  |
| maintenance instructions |[de](https://gitlab.com/ourplant.net/products/s3-0002-horizontal-inspection-unit-i-h38/-/raw/main/04_maintenance_instructions/S3-0002_A_Wartungsanweisungen.pdf)|
| spare parts              |  [de](https://gitlab.com/ourplant.net/products/s3-0002-horizontal-inspection-unit-i-h38/-/raw/main/05_spare_parts/S3-0002_B2_EVL.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0002-horizontal-inspection-unit-i-h38/-/raw/main/05_spare_parts/S3-0002_B2_EVL_engl.pdf)|

